import './App.css';
import {Container} from 'react-bootstrap'


import Counter from './Counter'

function App() {
  return (
    <>
    <Container>
    <Counter />
    </Container>
    </>
  );
}

export default App;
