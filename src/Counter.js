import { useState } from 'react'
import CounterDisplay from './CounterDisplay'
import {Container, Row, Col, Button} from 'react-bootstrap'

function Counter() {
  
  const [count, setCount] = useState(0)
  
  const increment = () => {
    setCount(count + 1)
  }
  const decrement = () => {
    setCount(count - 1)
  }
  
  return (
    <Container className="vh-100 d-flex flex-column justify-content-center">
    <Row className="my-3 justify-content-center">
      <Col xs="auto">
        <CounterDisplay count={count} />
      </Col>
    </Row>
    <Row className="my-3 justify-content-center">
      <Col xs="auto">
        <Button variant="primary" onClick={increment}>
          Increment
        </Button>
      </Col>
      <Col xs="auto">
        <Button variant="danger" onClick={decrement}>
          Decrement
        </Button>
      </Col>
    </Row>
  </Container>
  )
  
}

export default Counter