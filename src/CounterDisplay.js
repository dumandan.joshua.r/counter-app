import React from 'react'

function CounterDisplay ({count}) {
  return (
    <div>
      <h2>Counter Display: {count}</h2>
    </div>
  )
}

export default CounterDisplay